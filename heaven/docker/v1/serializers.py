from flask import jsonify
import functools
from docker.models.resource import Collection
from docker.errors import NotFound


class JsonApiSerializer:
    def serialize(self, data):
        if isinstance(data, Collection):
            items = [self.serialize(item) for item in data.list()]
            return items

        return data.attrs


def ApiSerialize(entity=''):
    def decorated(func):
        @functools.wraps(func)
        def decorator(*args, **kwargs):
            try:
                return jsonify(JsonApiSerializer().serialize(func(*args, **kwargs)))
            except NotFound:
                return jsonify(data={
                    'msg': ("%s not found" % entity),
                    'code': '404'
                })
        return decorator
    return decorated
