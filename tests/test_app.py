import unittest

from heaven import app


class TestCase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        self.client = app.test_client()

    def test_helloWorld(self):
        rv = self.client.get('/')
        assert b'Hello, Jan' in rv.data


if __name__ == "__main__":
    unittest.main()
