FROM python:3.7-slim

ENV FLASK_APP=/app/run.py

WORKDIR /app
COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

COPY . /app

CMD ["flask", "run", "--host=0.0.0.0"]
