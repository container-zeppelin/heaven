import docker
from flask import Blueprint
from .serializers import ApiSerialize
from docker.errors import NotFound


client = docker.from_env()
dockerApi = Blueprint('docker', __name__)

""" 
Containers
"""
@dockerApi.route('/containers/')
@ApiSerialize(entity='container')
def index_containers():
    return client.containers


@dockerApi.route('/containers/<container_id>')
@ApiSerialize(entity='container')
def get_container(container_id):
    return client.containers.get(container_id)


"""
Images
"""
@dockerApi.route('/images/')
@ApiSerialize(entity='image')
def index_images():
    return client.images


@dockerApi.route('/images/<image_id>')
@ApiSerialize(entity='image')
def get_image(image_id):
    return client.images.get(image_id)


"""
Networks
"""
@dockerApi.route('/networks/')
@ApiSerialize(entity='network')
def index_networks():
    return client.networks


@dockerApi.route('/networks/<network_id>')
@ApiSerialize(entity='network')
def get_network(network_id):
    return client.networks.get(network_id)