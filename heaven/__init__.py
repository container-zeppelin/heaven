from flask import Flask, jsonify
from heaven.docker.v1.controllers import dockerApi

app = Flask(__name__)

app.register_blueprint(dockerApi, url_prefix='/api/v1/docker')


@app.route('/')
def hello_world():
    return jsonify('Hello, Jan')